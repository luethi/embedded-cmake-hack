#include "main_app.h"
#include "board/board.h"
#include "util/logger.h"
#include "task/measure_temperature.h"

#include "thread.hpp"
#include "ticks.hpp"

namespace rtos = cpp_freertos;

class TestThread : public rtos::Thread
{
public:
    TestThread(std::string name, int i, int delayInMs)
        : Thread(name, 256, 1),
          id(i),
          DelayInMs(delayInMs)
    {
        Start();
    };

protected:
    virtual void Run()
    {
        LOG_INFO("Starting thread %X %s", id, GetName().c_str());

        while (true)
        {
            TickType_t ticks = rtos::Ticks::MsToTicks(DelayInMs);

            switch (id)
            {
            case 1:
                Board::instance().led.state.toggle();
                Board::instance().actuator.boiler.toggle();
                Board::instance().actuator.valve.toggle();
                //LOG_INFO("Hi there.");
                break;
            case 2:
                Board::instance().led.com.toggle();
                break;
            default:
                break;
            }

            if (ticks)
                Delay(ticks);
        }
    };

private:
    int id;
    int DelayInMs;
};

void main_app()
{
    Board::instance().actuator.pump.start();
    static TestThread thread1("Thr_1", 1, 1000);
    static TestThread thread2("Thr_2", 2, 100);
    static Measure_temperature measure_temperature;
}