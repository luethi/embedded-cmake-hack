#pragma once

#include "temperature_base.h"
#include "board/interface/spi.h"
#include "board/interface/gpio.h"
#include "ad7124.h"

class Temperature_ad7124 final : public I_temperature_sensor {
public:
    Temperature_ad7124(const I_spi& _spi, const I_gpio& _gpio);
    void init();
    float read_temperature(uint8_t id);

private:
    float calculate_resistance(int32_t value_raw, int32_t gain) const;
    float calculate_temperature(float resistance) const;
    void select_channel(uint32_t channel);

    Ad7124& adc;
    uint32_t reference_resistor;
    uint32_t adc_resolution;
    float rtd_base_resistance;
    float rtd_coefficient;
    uint32_t n_channel;
};
