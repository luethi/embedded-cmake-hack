/**
* @file     ad7124.cpp
* @author   Stefan Lüthi
*
* Based on Analog Devices open-source driver
 */

#include "ad7124.h"
#include "config/rtd100_5.h"
#include <string.h>
#include <cmath>

/* TODO: remove this nonsense */
/* Error codes */
#define INVALID_VAL -1 /* Invalid argument */
#define COMM_ERR    -2 /* Communication error on receive */
#define TIMEOUT     -3 /* A timeout has occured */

Ad7124::Ad7124(const I_spi& _spi, const I_gpio& _chip_select)
        :
        spi(_spi), chip_select(_chip_select)
{
}

void Ad7124::init()
{
    setup(regs_config_rtd100_5);
}

/* methods from Analog Devices                                                */
/*******************************************************************************
* Copyright 2015-2019(c) Analog Devices, Inc.
*
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*  - Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*  - Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in
*    the documentation and/or other materials provided with the
*    distribution.
*  - Neither the name of Analog Devices, Inc. nor the names of its
*    contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*  - The use of this software may or may not infringe the patent rights
*    of one or more patent holders.  This license does not release you
*    from the requirement that you obtain separate licenses from these
*    patent holders to use this software.
*  - Use of the software either in source or binary form, must be run
*    on or directly connected to an Analog Devices Inc. component.
*
* THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, MERCHANTABILITY
* AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL ANALOG DEVICES BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* INTELLECTUAL PROPERTY RIGHTS, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @brief Reads the value of the specified register without checking if the
 *        device is ready to accept user requests.
 *
 * @param reg  - Pointer to the register structure holding info about the
 *               register to be read. The read value is stored inside the
 *               register structure.
 *
 * @return Returns 0 for success or negative error code.
 */
int32_t Ad7124::no_check_read_register(struct ad7124_st_reg* reg)
{
    int32_t ret = 0;
    uint8_t buffer_tx[8] = {0}, buffer_rx[8] = {0};
    uint8_t i = 0;
    uint8_t check8 = 0;
    uint8_t add_status_length = 0;
    uint8_t msg_buf[8] = {0, 0, 0, 0, 0, 0, 0, 0};

    if (!reg)
        return INVALID_VAL;

    /* Build the Command word */
    buffer_tx[0] = AD7124_COMM_REG_WEN | AD7124_COMM_REG_RD |
            AD7124_COMM_REG_RA(reg->addr);

    /*
	 * If this is an AD7124_DATA register read, and the DATA_STATUS bit is set
	 * in ADC_CONTROL, need to read 4, not 3 bytes for DATA with STATUS
	 */
    if ((reg->addr==AD7124_DATA_REG) &&
            (regs[AD7124_ADC_Control].value & AD7124_ADC_CTRL_REG_DATA_STATUS)) {
        add_status_length = 1;
    }

    /* Read data from the device */
    ret = 0;
    chip_select.set(true);
    spi.transmit_and_receive_blocking(
            buffer_tx,
            buffer_rx,
            (use_crc ? reg->size+1
                     : reg->size)+1+add_status_length);
    chip_select.set(false);

    if (ret<0)
        return ret;

    /* Check the CRC */
    if (use_crc) {
        msg_buf[0] = AD7124_COMM_REG_WEN | AD7124_COMM_REG_RD |
                AD7124_COMM_REG_RA(reg->addr);
        for (i = 1; i<reg->size+2+add_status_length; ++i) {
            msg_buf[i] = buffer_rx[i];
        }
        check8 = compute_crc8(msg_buf, reg->size+2+add_status_length);
    }

    if (check8!=0) {
        /* ReadRegister checksum failed. */
        return COMM_ERR;
    }

    /*
     * if reading Data with 4 bytes, need to copy the status byte to the STATUS
     * register struct value member
     */
    if (add_status_length) {
        regs[AD7124_Status].value = buffer_rx[reg->size+1];
    }

    /* Build the result */
    reg->value = 0;
    uint32_t val = 0;
    for (i = 1; i<reg->size+1; i++) {
        reg->value <<= 8;
        reg->value += buffer_rx[i];

        val <<= 8;
        val += buffer_rx[i];
    }

    return ret;
}

/**
 * @brief Writes the value of the specified register without checking if the
 *        device is ready to accept user requests.
 *
 * @param reg - Register structure holding info about the register to be written
 *
 * @return Returns 0 for success or negative error code.
 */
int32_t Ad7124::no_check_write_register(struct ad7124_st_reg reg)
{
    int32_t ret = 0;
    int32_t reg_value = 0;
    uint8_t buffer_tx[8] = {0};
    uint8_t buffer_rx[8] = {0};
    uint8_t i = 0;
    uint8_t crc8 = 0;

    /* Build the Command word */
    buffer_tx[0] = AD7124_COMM_REG_WEN | AD7124_COMM_REG_WR |
            AD7124_COMM_REG_RA(reg.addr);

    /* Fill the write buffer */
    reg_value = reg.value;
    for (i = 0; i<reg.size; i++) {
        buffer_tx[reg.size-i] = reg_value & 0xFF;
        reg_value >>= 8;
    }

    /* Compute the CRC */
    if (use_crc!=AD7124_DISABLE_CRC) {
        crc8 = compute_crc8(buffer_tx, reg.size+1);
        buffer_tx[reg.size+1] = crc8;
    }

    /* Write data to the device */
    chip_select.set(true);
    spi.transmit_blocking(
            buffer_tx,
            use_crc ? reg.size+2
                    : reg.size+1);
    chip_select.set(false);
    ret = 0;

    return ret;
}

/**
 * @brief Reads the value of the specified register only when the device is ready
 *        to accept user requests. If the device ready flag is deactivated the
 *        read operation will be executed without checking the device state.
 *
 * @param reg   - Pointer to the register structure holding info about the
 *               register to be read. The read value is stored inside the
 *               register structure.
 *
 * @return Returns 0 for success or negative error code.
 */
int32_t Ad7124::read_register(struct ad7124_st_reg* reg)
{
    int32_t ret;

    if (reg->addr!=AD7124_ERR_REG && check_device_ready) {
        ret = wait_for_spi_ready(spi_rdy_poll_cnt);
        if (ret<0)
            return ret;
    }
    ret = no_check_read_register(reg);

    return ret;
}

/**
 * @brief Writes the value of the specified register only when the device is
 *        ready to accept user requests. If the device ready flag is deactivated
 *        the write operation will be executed without checking the device state.
 *
 * @param reg - Register structure holding info about the register to be written
 *
 * @return Returns 0 for success or negative error code.
 */
int32_t Ad7124::write_register(struct ad7124_st_reg reg)
{
    int32_t ret;

    if (check_device_ready) {
        ret = wait_for_spi_ready(spi_rdy_poll_cnt);
        if (ret<0)
            return ret;
    }
    ret = no_check_write_register(reg);

    return ret;
}

/**
 * @brief Resets the device.
 *
 * @return Returns 0 for success or negative error code.
 */
int32_t Ad7124::reset()
{
    int32_t ret = 0;
    uint8_t buffer_tx[8] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

    chip_select.set(true);
    spi.transmit_blocking(buffer_tx, 8);
    chip_select.set(false);

    /* CRC is disabled after reset */
    use_crc = false;

    /* Read POR bit to clear */
    ret = wait_to_power_on(spi_rdy_poll_cnt);

    //mdelay(AD7124_POST_RESET_DELAY);

    return ret;
}

/**
 * @brief Waits until the device can accept read and write user actions.
 *
 * @param timeout - Count representing the number of polls to be done until the
 *                  function returns.
 *
 * @return Returns 0 for success or negative error code.
 */
int32_t Ad7124::wait_for_spi_ready(uint32_t timeout)
{
    int32_t ret;
    int8_t ready = 0;

    while (!ready && --timeout) {
        /* Read the value of the Error Register */
        ret = read_register(&regs[AD7124_Error]);
        if (ret<0)
            return ret;

        /* Check the SPI IGNORE Error bit in the Error Register */
        ready = (regs[AD7124_Error].value &
                AD7124_ERR_REG_SPI_IGNORE_ERR)==0;
    }

    return timeout ? 0 : TIMEOUT;
}

/**
 * @brief Waits until the device finishes the power-on reset operation.
 *
 * @param timeout - Count representing the number of polls to be done until the
 *                  function returns.
 *
 * @return Returns 0 for success or negative error code.
 */
int32_t Ad7124::wait_to_power_on(uint32_t timeout)
{
    int32_t ret;
    int8_t powered_on = 0;

    while (!powered_on && timeout--) {
        ret = read_register(&regs[AD7124_Status]);
        if (ret<0)
            return ret;

        /* Check the POR_FLAG bit in the Status Register */
        powered_on = (regs[AD7124_Status].value &
                AD7124_STATUS_REG_POR_FLAG)==0;
    }

    return (timeout || powered_on) ? 0 : TIMEOUT;
}

/**
 * @brief Waits until a new conversion result is available.
 *
 * @param timeout - Count representing the number of polls to be done until the
 *                  function returns if no new data is available.
 *
 * @return Returns 0 for success or negative error code.
 */
int32_t Ad7124::wait_for_conv_ready(uint32_t timeout)
{
    int32_t ret;
    int8_t ready = 0;

    while (!ready && --timeout) {
        /* Read the value of the Status Register */
        ret = read_register(&regs[AD7124_Status]);
        if (ret<0)
            return ret;

        /* Check the RDY bit in the Status Register */
        ready = (regs[AD7124_Status].value &
                AD7124_STATUS_REG_RDY)==0;
    }

    return timeout ? 0 : TIMEOUT;
}

/**
 * @brief Reads the conversion result from the device.
 *
 * @param data  - Pointer to store the read data.
 *
 * @return Returns 0 for success or negative error code.
 */
int32_t Ad7124::read_data(int32_t* data)
{
    int32_t ret;

    /* Read the value of the Status Register */
    ret = read_register(&regs[AD7124_Data]);

    /* Get the read result */
    *data = regs[AD7124_Data].value;

    return ret;
}

/**
 * @brief Computes the CRC checksum for a data buffer.
 *
 * @param buffer  - Data buffer
 * @param size    s- Data buffer size in bytes
 *
 * @return Returns the computed CRC checksum.
 */
uint8_t Ad7124::compute_crc8(uint8_t* buffer, uint8_t size)
{
    uint8_t i = 0;
    uint8_t crc = 0;

    while (size) {
        for (i = 0x80; i!=0; i >>= 1) {
            bool cmp1 = (crc & 0x80)!=0;
            bool cmp2 = (*buffer & i)!=0;
            if (cmp1!=cmp2) { /* MSB of CRC register XOR input Bit from Data */
                crc <<= 1;
                crc ^= AD7124_CRC8_POLYNOMIAL_REPRESENTATION;
            }
            else {
                crc <<= 1;
            }
        }
        buffer++;
        size--;
    }
    return crc;
}

/**
 * @brief Updates the CRC settings.
 *
 * @return None.
 */
void Ad7124::update_crcsetting()
{
    /* Get CRC State. */
    if (regs[AD7124_Error_En].value & AD7124_ERREN_REG_SPI_CRC_ERR_EN) {
        use_crc = true;
    }
    else {
        use_crc = false;
    }
}

/**
 * @brief Updates the device SPI interface settings.
 *
 * @return None.
 */
void Ad7124::update_dev_spi_settings()
{
    if (regs[AD7124_Error_En].value & AD7124_ERREN_REG_SPI_IGNORE_ERR_EN) {
        check_device_ready = true;
    }
    else {
        check_device_ready = false;
    }
}

/**
 * @brief Initializes the AD7124.
 *
 *
 * @return Returns 0 for success or negative error code.
 */
int32_t Ad7124::setup(const struct ad7124_st_reg* _regs)
{
    int32_t ret;
    //enum ad7124_registers reg_nr;
    int32_t reg_nr;

    memcpy(regs, _regs, sizeof(regs));
    //spi_rdy_poll_cnt = init_param.spi_rdy_poll_cnt;
    spi_rdy_poll_cnt = 100;

    /*  Reset the device interface.*/
    ret = reset();
    if (ret<0)
        return ret;

    /* Update the device structure with power-on/reset settings */
    //check_device_ready = true; // todo: this cannot work!

    /* Initialize registers AD7124_ADC_Control through AD7124_Filter_7. */
    //for(reg_nr = AD7124_Status; (reg_nr < AD7124_Offset_0) && !(ret < 0); reg_nr++) {
    for (reg_nr = 0; reg_nr<41; reg_nr++) {
        if (regs[reg_nr].rw==AD7124_RW) {
            ret = write_register(regs[reg_nr]);
            if (ret<0)
                break;
        }

        /* Get CRC State and device SPI interface settings */
        if (reg_nr==AD7124_Error_En) {
            update_crcsetting();
            update_dev_spi_settings();
        }
    }

    return ret;
}

int32_t Ad7124::get_gain(uint32_t setup)
{
    return 1 << AD7124_CFG_REG_PGA(regs[AD7124_Config_0+setup].value);
}
