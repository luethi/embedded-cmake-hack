#include "temperature_ad7124.h"
#include "config/rtd100_5.h"
#include <string.h>
#include <cmath>

Temperature_ad7124::Temperature_ad7124(const I_spi& _spi, const I_gpio& _chip_select)
        :adc(*new Ad7124(_spi, _chip_select))
{
    // TODO: move these to flash
    reference_resistor = 5110;
    adc_resolution = 24;
    rtd_base_resistance = 100;
    rtd_coefficient = 0.385;
    n_channel = 5;

    init();
}

void Temperature_ad7124::init()
{
    adc.init();
}

float Temperature_ad7124::read_temperature(uint8_t id)
{
    int32_t value_raw;
    float temperature = 0.0;

    select_channel(id);

    int32_t ret = adc.wait_for_conv_ready(1000);
    if (ret>=0) {
        adc.read_data(&value_raw);
        if (value_raw != 0xFFFFFF) {
            uint32_t gain = adc.get_gain(0);
            float resistance = calculate_resistance(value_raw, gain);
            temperature =  calculate_temperature(resistance);
        } else {
            temperature = -1.0;
        }
    }
    return temperature;
}

float Temperature_ad7124::calculate_resistance(int32_t value_raw, int32_t gain) const
{
    return (((uint64_t) value_raw-(1 << (adc_resolution-1)))*reference_resistor)/
            (float) (gain*(1 << (adc_resolution-1)));
}

float Temperature_ad7124::calculate_temperature(float resistance) const
{
    return (resistance-rtd_base_resistance)/rtd_coefficient;
}

void Temperature_ad7124::select_channel(uint32_t channel)
{
    // activate input channel with config 0 and filter 0
    for (uint32_t i = 0; i<n_channel; ++i) {
        if (i==channel) {
            adc.regs[AD7124_Channel_0+i].value |= 0x8000;
        }
        else {
            adc.regs[AD7124_Channel_0+i].value &= ~0x8000;
        }
        adc.write_register(adc.regs[AD7124_Channel_0+i]);
    }
    // change current source
    adc.regs[AD7124_IOCon1].value &= ~0x000F;
    adc.regs[AD7124_IOCon1].value |= (channel*3) & 0x000F;
    adc.write_register(adc.regs[AD7124_IOCon1]);
}