#pragma once

#include "temperature_base.h"
#include "devdrv/ad7124/ad7124_regs.h"
#include "board/interface/spi.h"
#include "board/interface/gpio.h"

class Ad7124 final {
public:
    Ad7124(const I_spi& _spi, const I_gpio& _gpio);
    void init();

    int32_t no_check_read_register(struct ad7124_st_reg* reg);
    int32_t no_check_write_register(struct ad7124_st_reg reg);
    int32_t read_register(struct ad7124_st_reg* reg);
    int32_t write_register(struct ad7124_st_reg reg);
    int32_t reset();
    int32_t wait_for_spi_ready(uint32_t timeout);
    int32_t wait_to_power_on(uint32_t timeout);
    int32_t wait_for_conv_ready(uint32_t timeout);
    int32_t read_data(int32_t* data);
    uint8_t compute_crc8(uint8_t* buffer, uint8_t size);
    void update_crcsetting();
    void update_dev_spi_settings();
    int32_t setup(const struct ad7124_st_reg* regs);
    int32_t get_gain(uint32_t setup);

    struct ad7124_st_reg regs[AD7124_REG_NO];
private:
    const I_spi& spi;
    const I_gpio& chip_select;

    bool check_device_ready = false;
    uint32_t spi_rdy_poll_cnt = 0;
    bool use_crc = false;
};