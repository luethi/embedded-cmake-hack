#pragma once

#include <stdint.h>

class I_temperature_sensor
{
public:
    virtual void init() = 0;
    virtual float read_temperature(uint8_t id) = 0;
};
