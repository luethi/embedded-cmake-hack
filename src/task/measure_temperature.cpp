#include "task/measure_temperature.h"
#include "driver/temperature_ad7124/temperature_ad7124.h"

#include "board/board.h"
#include "util/logger.h"

#include "thread.hpp"
#include "ticks.hpp"

Measure_temperature::Measure_temperature()
        :Thread("Measure temperature", 256, 1),
         sensor(*new Temperature_ad7124(Board::instance().interface.adc.spi,
                 Board::instance().interface.adc.cs_adc))
{
    Start();
};

void Measure_temperature::Run()
{
    LOG_INFO("Starting thread measure temperature");
    TickType_t ticks = rtos::Ticks::MsToTicks(500);

    while (true) {
        for (uint32_t i = 0; i < 5; ++i) {
            float temp = sensor.read_temperature(i);
            LOG_INFO("T%d = %d.%02d", i+1, (int32_t) temp, abs((temp-(int32_t) temp)*100));
        }
        if (ticks)
            Delay(ticks);
    }
};


