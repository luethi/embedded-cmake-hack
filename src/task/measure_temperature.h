#pragma once

#include "driver/temperature_ad7124/temperature_base.h"

#include "thread.hpp"

namespace rtos = cpp_freertos;

class Measure_temperature : public rtos::Thread
{
public:
    Measure_temperature();

protected:
    void Run() override;

private:
    I_temperature_sensor& sensor;
};
