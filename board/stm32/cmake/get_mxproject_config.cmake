function(get_mxproject_config 
         mxproject SOURCE_FILES INCLUDE_DIRS DEFINITIONS)
	file(READ "${mxproject}" file_mxproject)

	set(REGEX_SYMBOLS "[A-Za-z0-9_\\./;]*")

	#string(REGEX MATCH "SourcePath=${REGEX_SYMBOLS}" source_path "${file_mxproject}")
	#string(REPLACE "SourcePath=" "" source_path "${source_path}")
	set(source_path ${CMAKE_CURRENT_SOURCE_DIR}/Src)
	set(header_path ${CMAKE_CURRENT_SOURCE_DIR}/Inc)

	# find all source file lists
	string(REGEX MATCHALL "SourceFiles=${REGEX_SYMBOLS}" source_files "${file_mxproject}")
	string(REPLACE "null;" "" source_files "${source_files}")
	# remove weird fiels
	string(REPLACE "../Drivers/CMSIS/Device/ST/STM32F3xx/Source/Templates/system_stm32f3xx.c;" "" source_files "${source_files}")
	# add source path to files
	string(REGEX REPLACE "[A-Za-z0-9_\\./]*;" "${source_path}/\\0" source_files "${source_files}")
	string(REPLACE "SourceFiles=" "" source_files "${source_files}")

	# find all include directories
	# first header path is prefix
	#string(REGEX MATCH "HeaderPath=${REGEX_SYMBOLS}" header_path "${file_mxproject}")
	string(REGEX MATCHALL "HeaderPath=${REGEX_SYMBOLS}" header_paths "${file_mxproject}")
	string(REPLACE "${header_path};" "" header_paths "${header_paths}")
	# add header path prefix to files
	#string(REPLACE "HeaderPath=" "" header_path "${header_path}")
	string(REGEX REPLACE "[A-Za-z0-9_\\.\/]*;" "${header_path}/\\0" header_paths "${header_paths}")
	string(REPLACE "HeaderPath=" "" header_paths "${header_paths}")

	# find definitions
	string(REGEX MATCH "CDefines=[A-Za-z0-9_\(\)\":;]*" defines "${file_mxproject}")
	string(REPLACE "CDefines=" "" defines "${defines}")
	string(REGEX REPLACE "[A-Za-z0-9_\(\)\":]*;" "-D\\0" defines "${defines}")
    string(REPLACE ":" "=" defines "${defines}")
	string(REPLACE "\"" "" defines "${defines}")

	set(${SOURCE_FILES} "${source_files}" PARENT_SCOPE)
	set(${INCLUDE_DIRS} "${header_paths}" PARENT_SCOPE)
	set(${DEFINITIONS}  "${defines}" PARENT_SCOPE)
endfunction()
