/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f3xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void SystemClock_Config(void);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define var1_Pin GPIO_PIN_1
#define var1_GPIO_Port GPIOC
#define var2_Pin GPIO_PIN_2
#define var2_GPIO_Port GPIOC
#define var3_Pin GPIO_PIN_3
#define var3_GPIO_Port GPIOC
#define pressure_Pin GPIO_PIN_2
#define pressure_GPIO_Port GPIOA
#define cs_adc_Pin GPIO_PIN_4
#define cs_adc_GPIO_Port GPIOA
#define valve_Pin GPIO_PIN_13
#define valve_GPIO_Port GPIOB
#define pump_Pin GPIO_PIN_14
#define pump_GPIO_Port GPIOB
#define boiler_Pin GPIO_PIN_15
#define boiler_GPIO_Port GPIOB
#define btn_user_Pin GPIO_PIN_6
#define btn_user_GPIO_Port GPIOC
#define error_Pin GPIO_PIN_8
#define error_GPIO_Port GPIOC
#define state_Pin GPIO_PIN_9
#define state_GPIO_Port GPIOC
#define com_Pin GPIO_PIN_8
#define com_GPIO_Port GPIOA
#define zc_Pin GPIO_PIN_10
#define zc_GPIO_Port GPIOA
#define btn_brew_Pin GPIO_PIN_11
#define btn_brew_GPIO_Port GPIOC
#define btn_steam_Pin GPIO_PIN_12
#define btn_steam_GPIO_Port GPIOC
#define btn_water_Pin GPIO_PIN_2
#define btn_water_GPIO_Port GPIOD
#define btn_power_Pin GPIO_PIN_4
#define btn_power_GPIO_Port GPIOB
#define led_power_Pin GPIO_PIN_5
#define led_power_GPIO_Port GPIOB
#define flow_Pin GPIO_PIN_9
#define flow_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
