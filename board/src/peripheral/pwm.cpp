#include "board/peripheral/pwm.h"

Pwm::Pwm(timer_handle_t _handle) : Timer{_handle} 
{

}

void Pwm::start() const
{
    Timer::start();
    HAL_TIM_PWM_Start(Timer::handle, TIM_CHANNEL_1);
}

void Pwm::stop() const
{

}