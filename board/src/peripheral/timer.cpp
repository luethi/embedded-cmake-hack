#include "board/peripheral/timer.h"

Timer::Timer(timer_handle_t _handle) : handle(_handle) 
{
    
}

void Timer::start() const
{
    HAL_TIM_Base_Start(handle);
}

void Timer::stop() const
{
    HAL_TIM_Base_Stop(handle);
}