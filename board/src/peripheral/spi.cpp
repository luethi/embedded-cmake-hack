#include "board/peripheral/spi.h"

Spi::Spi(spi_handle_t _handle) : handle(_handle) 
{
    
}

void Spi::transmit_blocking(uint8_t *data, uint32_t size) const
{
    HAL_SPI_Transmit(handle, data, size, 1000);
}

void Spi::transmit_and_receive_blocking(uint8_t *data_tx, uint8_t *data_rx, uint32_t size) const
{
    HAL_SPI_TransmitReceive(handle, data_tx, data_rx, size, 1000);
}

void Spi::receive_blocking(uint8_t *data, uint32_t size) const
{
    HAL_SPI_Receive(handle, data, size, 1000);
}