#include "board/peripheral/gpio.h"
#include "stm32f3xx_hal.h"

Gpio::Gpio(port_t _port, pin_t _pin) : Gpio::Gpio(_port, _pin, false)
{
}

Gpio::Gpio(port_t _port, pin_t _pin, bool _active_low) :
    port(_port), pin(_pin), active_low(_active_low)
{
}

void Gpio::set(bool on) const
{
    HAL_GPIO_WritePin(
        port, 
        pin, 
        on ^ active_low ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

bool Gpio::get() const
{
    return HAL_GPIO_ReadPin(port, pin) ^ active_low;
}

void Gpio::toggle() const
{
    HAL_GPIO_TogglePin(port, pin);
}
