#include "board/board.h"
#include "util/logger.h"
#include "util/swo.h"

#include "main_app.h"
#include "thread.hpp"
#include "ticks.hpp"


namespace rtos = cpp_freertos;

class Main_thread : public rtos::Thread
{
public:
    Main_thread() : Thread("main", 256, 1)
    {
        Start();
    };

protected:
    virtual void Run()
    {
        main_app();
        LOG_INFO("Suspending main app");
        Suspend();
    };
};

void logger_print(const char *s)
{
    SWO_print_string(s, 1);
}

void main()
{
    Board::instance().init();

    LOG_init(&logger_print);
    LOG_log_level_set(LOG_LEVEL_DEBUG);
    LOG_INFO("Espresso Master 2000");
    LOG_INFO("Build: %s, %s", __DATE__, __TIME__);
    LOG_INFO("HW varaint: %d", Board::instance().hw_variant.get());

    static Main_thread main_thread;
    rtos::Thread::StartScheduler();

    LOG_ERROR("Nobody expects the spanish inquisition!");
    while (1)
        asm("");
}