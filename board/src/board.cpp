
#include "board/peripheral/gpio.h"
#include "board/peripheral/pwm.h"
#include "board/peripheral/spi.h"

/* HAL dependencies */
#include "main.h"
#include "adc.h"
#include "gpio.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "usb_device.h"
#include "usbd_cdc.h"
#include "stm32f3xx_hal.h"

#include <stdint.h>

#include "board/board.h"

Board& Board::instance()
{
    static Board m_instance;
    return m_instance;
}

Board::Board()
        :
        led({
                *new Gpio((port_t) state_GPIO_Port, (pin_t) state_Pin),
                *new Gpio((port_t) com_GPIO_Port, (pin_t) com_Pin),
                *new Gpio((port_t) error_GPIO_Port, (pin_t) error_Pin),
        }),
        panel({
                {
                        *new Gpio((port_t) btn_power_GPIO_Port, (pin_t) btn_power_Pin),
                        *new Gpio((port_t) btn_brew_GPIO_Port, (pin_t) btn_brew_Pin),
                        *new Gpio((port_t) btn_water_GPIO_Port, (pin_t) btn_water_Pin),
                        *new Gpio((port_t) btn_steam_GPIO_Port, (pin_t) btn_steam_Pin),
                },
                {
                        *new Gpio((port_t) led_power_GPIO_Port, (pin_t) led_power_Pin),
                }
        }),
        actuator({
                *new Gpio((port_t) valve_GPIO_Port, (pin_t) valve_Pin),
                *new Gpio((port_t) boiler_GPIO_Port, (pin_t) boiler_Pin),
                *new Pwm(&htim15),
        }),
        hw_variant({
                *new Gpio((port_t) var1_GPIO_Port, (pin_t) var1_Pin),
                *new Gpio((port_t) var2_GPIO_Port, (pin_t) var2_Pin),
                *new Gpio((port_t) var3_GPIO_Port, (pin_t) var3_Pin),
        }),
        interface({
                {
                        *new Spi(&hspi1),
                        *new Gpio((port_t) cs_adc_GPIO_Port, (pin_t) cs_adc_Pin, true),
                }

        })
{

}

void Board::init()
{
    HAL_Init();
    SystemClock_Config();
    MX_GPIO_Init();
    MX_ADC1_Init();
    MX_SPI1_Init();
    MX_TIM1_Init();
    MX_TIM15_Init();
    MX_TIM17_Init();
    MX_USART1_UART_Init();
    MX_USB_DEVICE_Init();
    //MX_RTC_Init();
    Board::instance();
}