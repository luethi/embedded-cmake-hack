#pragma once

#include <catch2/catch.hpp>
#include <catch2/trompeloeil.hpp>

#include "board/interface/spi.h"

class Spi_mock : public trompeloeil::mock_interface<I_spi>
{
public:
    IMPLEMENT_CONST_MOCK2(transmit_blocking);
    IMPLEMENT_CONST_MOCK3(transmit_and_receive_blocking);
    IMPLEMENT_CONST_MOCK2(receive_blocking);
};