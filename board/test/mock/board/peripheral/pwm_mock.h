#pragma once

#include <catch2/catch.hpp>
#include <catch2/trompeloeil.hpp>

#include "board/interface/pwm.h"

class Pwm_mock : public trompeloeil::mock_interface<I_pwm>
{
public:
    IMPLEMENT_CONST_MOCK0(start);
    IMPLEMENT_CONST_MOCK0(stop);
};