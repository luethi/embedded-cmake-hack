/* Gpio MOCK */

#pragma once

#include <catch2/catch.hpp>
#include <catch2/trompeloeil.hpp>

#include "board/interface/gpio.h"

typedef uint32_t port_t;
typedef uint32_t pin_t;

class Gpio_mock : public trompeloeil::mock_interface<I_gpio>
{
public:
    static constexpr bool trompeloeil_movable_mock = true;
    
    IMPLEMENT_CONST_MOCK1(set);
    IMPLEMENT_CONST_MOCK0(get);
    IMPLEMENT_CONST_MOCK0(toggle);
};