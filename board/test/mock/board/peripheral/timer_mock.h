#pragma once

#include <catch2/catch.hpp>
#include <catch2/trompeloeil.hpp>

#include "board/interface/timer.h"

class Timer_mock : public trompeloeil::mock_interface<I_timer>
{
public:
    IMPLEMENT_CONST_MOCK0(start);
    IMPLEMENT_CONST_MOCK0(stop);
};