#include "board/board.h"
#include "board/peripheral/gpio_mock.h"
#include "board/peripheral/pwm_mock.h"
#include "board/peripheral/spi_mock.h"

Board& Board::instance()
{
    static Board m_instance;
    return m_instance;
}

Board::Board(void) :
    led({
        *new Gpio_mock(),
        *new Gpio_mock(),
        *new Gpio_mock(),
    }),
    panel({
        {
            *new Gpio_mock(),
            *new Gpio_mock(),
            *new Gpio_mock(),
            *new Gpio_mock(),
        },
        {
            *new Gpio_mock(),
        }
    }),
    actuator({
        *new Gpio_mock(),
        *new Gpio_mock(),
        *new Pwm_mock(),
    }),
    hw_variant({
        *new Gpio_mock(),
        *new Gpio_mock(),
        *new Gpio_mock(),
    }),
    interface({
            {
                    *new Spi_mock(),
                    *new Gpio_mock(),
            }
    })
{

}

void Board::init(void)
{

}