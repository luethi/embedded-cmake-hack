#pragma once

#include <stdint.h>

#include "board/interface/gpio.h"
#include "board/interface/pwm.h"
#include "board/interface/spi.h"

class Board {
public:
    static Board& instance();
    void init();

    Board(Board const&) = delete;
    void operator=(Board const&) = delete;

    /* io groups and maps */
    const struct {
      I_gpio& state;
      I_gpio& com;
      I_gpio& error;
    } led;

    const struct {
      const struct {
        I_gpio& power;
        I_gpio& brew;
        I_gpio& water;
        I_gpio& steam;
      } button;
      const struct {
        I_gpio& power;
      } led;
    } panel;

    const struct {
      I_gpio& valve;
      I_gpio& boiler;
      I_pwm& pump;
    } actuator;

    const struct variant_struct {
      I_gpio& pin_0;
      I_gpio& pin_1;
      I_gpio& pin_2;

      uint8_t get() const
      {
          return pin_0.get() | pin_1.get() << 1 | pin_2.get() << 2;
      }
    } hw_variant;

    const struct {
      const struct {
        I_spi& spi;
        I_gpio& cs_adc;
      } adc;
    } interface;

private:
    Board();
};