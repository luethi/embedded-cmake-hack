#pragma once

#include "board/interface/spi.h"
#include "stm32f3xx_hal.h"

typedef SPI_HandleTypeDef* spi_handle_t;

class Spi final : public I_spi
{
public:
    Spi(spi_handle_t handle);
    void transmit_blocking(uint8_t *data, uint32_t size) const;
    void transmit_and_receive_blocking(uint8_t *data_tx, uint8_t *data_rx, uint32_t size) const;
    void receive_blocking(uint8_t *data, uint32_t size) const;

private:
    const spi_handle_t handle;
};
