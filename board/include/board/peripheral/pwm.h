#pragma once

#include "board/interface/pwm.h"
#include "board/peripheral/timer.h"
#include "stm32f3xx_hal.h"

class Pwm final : public I_pwm, public Timer
{
public:
    Pwm(timer_handle_t handle);
    void start() const;
    void stop() const;
};
