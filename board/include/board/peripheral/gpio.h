#pragma once

#include "board/interface/gpio.h"
#include "stm32f3xx_hal.h"

typedef GPIO_TypeDef* port_t;
typedef uint16_t pin_t;

class Gpio final : public I_gpio
{
public:
    Gpio(port_t port, pin_t pin);
    Gpio(port_t port, pin_t pin, bool active_low);
    void set(bool on) const;
    bool get() const;
    void toggle() const;

private:
    const port_t port;
    const pin_t pin;
    const bool active_low;
};
