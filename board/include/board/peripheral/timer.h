#pragma once

#include "board/interface/timer.h"
#include "stm32f3xx_hal.h"

typedef TIM_HandleTypeDef* timer_handle_t;

class Timer : public I_timer
{
public:
    Timer(timer_handle_t handle);
    void start() const;
    void stop() const;

protected:
    const timer_handle_t handle;
};