#pragma once

#include <stdint.h>

class I_spi
{
public:
    virtual void transmit_blocking(uint8_t *data, uint32_t size) const = 0;
    virtual void transmit_and_receive_blocking(uint8_t *data_tx, uint8_t *data_rx, uint32_t size) const = 0;
    virtual void receive_blocking(uint8_t *data, uint32_t size) const = 0;
};
