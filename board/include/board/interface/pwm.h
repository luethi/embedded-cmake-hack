#pragma once

#include "board/interface/timer.h"
#include <stdint.h>

class I_pwm : public I_timer
{
public:
    virtual void start() const = 0;
    virtual void stop() const = 0;
};
