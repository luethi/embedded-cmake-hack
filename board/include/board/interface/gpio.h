
#pragma once

#include <stdint.h>

class I_gpio
{
public:
    virtual void set(bool on) const = 0;
    virtual bool get() const = 0;
    virtual void toggle() const = 0;
};
