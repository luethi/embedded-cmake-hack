#pragma once

#include <stdint.h>

class I_timer
{
public:
    virtual void start() const = 0;
    virtual void stop() const = 0;
};
