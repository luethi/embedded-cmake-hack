# CMake Template
## Main project module
### Basic project
```cmake
cmake_minimum_required(VERSION 3.14)
project(espresso-master CXX C ASM)

set(CMAKE_C_STANDARD   11)
set(CMAKE_CXX_STANDARD 14)
```

Differentiate between embedded and PC builds:
```cmake
if(${CMAKE_SYSTEM_PROCESSOR} MATCHES "arm")
    message("-- Building for embedded target")
    set(TARGET_SYSTEM "embedded")
else()
    message("-- Building for host")
    set(TARGET_SYSTEM "host")
endif()
```

### Compiler & Linker configuration
ToDo
```cmake

```

### Targets
Define empty targets depending whether build is for embedded or PC.
Sources will be added later.

```cmake
set(TARGET_EMBEDDED ${CMAKE_PROJECT_NAME})
set(TARGET_UNIT_TESTS ${CMAKE_PROJECT_NAME}-unit-tests)
set(TARGET_INTEGRATION_TESTS ${CMAKE_PROJECT_NAME}-integration-tests)

if(${TARGET_SYSTEM} MATCHES "embedded")
    add_executable(${TARGET_EMBEDDED})
    add_executable(${TARGET_INTEGRATION_TESTS} EXCLUDE_FROM_ALL)
    set(CMAKE_EXECUTABLE_SUFFIX .elf)

else(${TARGET_SYSTEM} MATCHES "host") 
    add_executable(${TARGET_UNIT_TESTS})
endif()
```

### Dependencies
Load the submodules from within the project and from remote with the 
`dependencies` module.
```cmake
add_subdirectory(board)
add_subdirectory(src)
add_subdirectory(dependencies)
```

Add sources and links to targets:
```cmake
if(${TARGET_SYSTEM} MATCHES "embedded")
    target_link_libraries(
        ${TARGET_EMBEDDED} PRIVATE 
        board::board
        src::src
        src::main
        src::rtos_interface 
        rtos::freertos_cpp
        devdrv::ad7124
        util::logger
        util::swo
    )

    target_link_libraries(
        ${TARGET_INTEGRATION_TESTS} PRIVATE 
        board::board
        src::src
        src::rtos_interface 
        rtos::freertos_cpp
        devdrv::ad7124
        util::logger
        util::swo
    )

else(${TARGET_SYSTEM} MATCHES "host") 
    target_link_libraries(
        ${TARGET_UNIT_TESTS} PRIVATE 
        board::mock
        src::src
        devdrv::ad7124
    )
endif()
```

### Testing
Load the testing sources and add them to the targets:
```cmake
enable_testing()
add_subdirectory(test)

if(${TARGET_SYSTEM} MATCHES "embedded")
    target_link_libraries(
        ${TARGET_INTEGRATION_TESTS} PRIVATE 
        test::integration
    )

else(${TARGET_SYSTEM} MATCHES "host") 
    target_link_libraries(
        ${TARGET_UNIT_TESTS} PRIVATE 
        test::unit
    )
    include(${catch_SOURCE_DIR}/contrib/Catch.cmake)
    catch_discover_tests(${TARGET_UNIT_TESTS})

endif()
```

### complete example
```cmake
# The MIT License (MIT) Copyright (c) 2020 Stefan Lüthi

cmake_minimum_required(VERSION 3.14)
project(espresso-master CXX C ASM)

set(CMAKE_C_STANDARD   11)
set(CMAKE_CXX_STANDARD 14)

if(${CMAKE_SYSTEM_PROCESSOR} MATCHES "arm")
    message("-- Building for embedded target")
    set(TARGET_SYSTEM "embedded")
else()
    message("-- Building for host")
    set(TARGET_SYSTEM "host")
endif()

# TODO: Update


set(TARGET_EMBEDDED ${CMAKE_PROJECT_NAME})
set(TARGET_UNIT_TESTS ${CMAKE_PROJECT_NAME}-unit-tests)
set(TARGET_INTEGRATION_TESTS ${CMAKE_PROJECT_NAME}-integration-tests)

if(${TARGET_SYSTEM} MATCHES "embedded")
    add_executable(${TARGET_EMBEDDED})
    add_executable(${TARGET_INTEGRATION_TESTS} EXCLUDE_FROM_ALL)
    set(CMAKE_EXECUTABLE_SUFFIX .elf)

else(${TARGET_SYSTEM} MATCHES "host") 
    add_executable(${TARGET_UNIT_TESTS})
endif()


add_subdirectory(board)
add_subdirectory(src)
add_subdirectory(dependencies)

if(${TARGET_SYSTEM} MATCHES "embedded")
    target_link_libraries(
        ${TARGET_EMBEDDED} PRIVATE 
        board::board
        src::src
        src::main
        src::rtos_interface 
        rtos::freertos_cpp
        devdrv::ad7124
        util::logger
        util::swo
    )

    target_link_libraries(
        ${TARGET_INTEGRATION_TESTS} PRIVATE 
        board::board
        src::src
        src::rtos_interface 
        rtos::freertos_cpp
        devdrv::ad7124
        util::logger
        util::swo
    )

else(${TARGET_SYSTEM} MATCHES "host") 
    target_link_libraries(
        ${TARGET_UNIT_TESTS} PRIVATE 
        board::mock
        src::src
        devdrv::ad7124
    )
endif()


enable_testing()
add_subdirectory(test)

if(${TARGET_SYSTEM} MATCHES "embedded")
    target_link_libraries(
        ${TARGET_INTEGRATION_TESTS} PRIVATE 
        test::integration
    )

else(${TARGET_SYSTEM} MATCHES "host") 
    target_link_libraries(
        ${TARGET_UNIT_TESTS} PRIVATE 
        test::unit
    )
    include(${catch_SOURCE_DIR}/contrib/Catch.cmake)
    catch_discover_tests(${TARGET_UNIT_TESTS})

endif()
```

## Submodule
The cmake file structure for a submodule is fairly similar to the main project 
module. The main difference is that the submodule only builds its tests if it 
build as module not as a submodule.

### Standalone/Submodule detection

```cmake
if(NOT DEFINED PROJECT_NAME)
  set(STANDALONE ON)
endif()

```

### Why INTERFACE?
A library can take some different forms, the typical one beeing:
```cmake
add_library(Library INTERFACE)
```
This will result in the library being built separately and the linked to
the target. This way only the linker can optimize the build.

By defining the library as interface
```cmake
add_library(Library INTERFACE)
```
the sources, compiler definitions and links are defined but
not build.

This leads to more room for the compiler to optimize the code in one big
compilation instead of multiple little ones.