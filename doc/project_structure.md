# Project Structure

- `board`: board support package, mainly a C++ wrapper for the STM32CubeMX 
generated HAL
- `cmake`: CMAke scripts used in the project
- `dependencies/CMakeLists.txt`: List of repos with CMake modules the project 
depends on. CMake will pull these repos (into `build-xy/_deps`). Use these for 
_finished_ libraries or other modules you do not intend to work on.
- `doc`: this documentation
- `lib`: Libraries and modules you will work on while developing the project,
load these modules as git submodules
- `remote`: configs for the debugger in use
- `src`: the actual project sources
- `test`: unit tests for modules in `src` as well as integration tests

The idea is to split the project in as many reusable modules as possible.

## Submodule
- `include/[topic]/[name].h`: this way the module can be included in a 
structured manner, i.e. `#include "driver/ad7124"`
- `src`: sources
- `test`: unit tests for the submodule