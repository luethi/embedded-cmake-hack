#include <catch2/catch.hpp>
#include <catch2/trompeloeil.hpp>

#include <string>

#include "board/board.h"
#include "board/peripheral/gpio_mock.h"

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

TEST_CASE("Test another module", "[classic]")
{
   SECTION("Test a stupid case") {
      //REQUIRE_CALL((Gpio_mock&) Board::instance().led.com, set(false));
      REQUIRE_CALL((Gpio_mock&) Board::instance().led.com, get()).RETURN(false);
      REQUIRE_FALSE(Board::instance().led.com.get());
   }
}