#include "util/cortex_timing.h"
#include "util/logger.h"

extern "C" {
#include "unity_fixture.h"
}

static void unity_run_all_tests(void)
{
    RUN_TEST_GROUP(temperature_ad7124)
    RUN_TEST_GROUP(some_other_test)
}

int main_app()
{
    CORTEX_TIM_init();
    const char* argv[] = {"", "-v"};
    return UnityMain(sizeof(argv[0]), argv, unity_run_all_tests);
}
