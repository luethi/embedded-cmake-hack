#include "unity.h"
extern "C" {
#include "unity_fixture.h"
}

TEST_GROUP(some_other_test);

TEST_SETUP(some_other_test)
{

}

TEST_TEAR_DOWN(some_other_test)
{
}

TEST(some_other_test, test_something)
{
    TEST_ASSERT_EQUAL_HEX8(80, 80);
    TEST_ASSERT_EQUAL_HEX8(80, 80);
}

TEST_GROUP_RUNNER(some_other_test)
{
    RUN_TEST_CASE(some_other_test, test_something)
}