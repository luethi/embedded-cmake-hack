#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#define CORTEX_DWT_CYCCNT_REG   ((unsigned int *) 0xE0001004)
#define TICKS_PER_US            72
#define UNITY_CLOCK_MS()        *CORTEX_DWT_CYCCNT_REG/TICKS_PER_US

#define UNITY_INCLUDE_EXEC_TIME
#define UNITY_TIME_TYPE UNITY_UINT
#define UNITY_EXEC_TIME_START() Unity.CurrentTestStartTime = UNITY_CLOCK_MS()
#define UNITY_EXEC_TIME_STOP() Unity.CurrentTestStopTime = UNITY_CLOCK_MS()
#define UNITY_PRINT_EXEC_TIME() { \
        UNITY_UINT execTimeMs = (Unity.CurrentTestStopTime - Unity.CurrentTestStartTime); \
        UnityPrint(" ("); \
        UnityPrintNumberUnsigned(execTimeMs); \
        UnityPrint(" us)"); \
        }

#ifdef __cplusplus
};
#endif