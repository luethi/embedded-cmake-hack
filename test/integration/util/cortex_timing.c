#include "cortex_timing.h"
#include "stm32f3xx.h"

// the LAR register cannot be found in ARM CMSIS struct
volatile uint32_t *DWT_LAR = (volatile uint32_t *)0xE0001FB0;

void CORTEX_TIM_init(void)
{
    CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
    DWT_LAR = (uint32_t*)0xC5ACCE55;
    DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
}