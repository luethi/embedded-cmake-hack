#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

void CORTEX_TIM_init(void);

#ifdef __cplusplus
};
#endif
