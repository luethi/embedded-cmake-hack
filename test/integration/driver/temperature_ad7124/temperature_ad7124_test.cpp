#include "unity.h"
extern "C" { // TODO: github ticket because they forgot to add the ifdef __cplusplus
#include "unity_fixture.h"
}
#include "board/board.h"
#include "driver/temperature_ad7124/temperature_base.h"
#include "driver/temperature_ad7124/temperature_ad7124.h"

TEST_GROUP(temperature_ad7124);

I_temperature_sensor* sensor;

TEST_SETUP(temperature_ad7124)
{
    sensor = new Temperature_ad7124(
            Board::instance().interface.adc.spi,
            Board::instance().interface.adc.cs_adc);
}

TEST_TEAR_DOWN(temperature_ad7124)
{
}

TEST(temperature_ad7124, test_temperature_probe_t1)
{
    TEST_ASSERT_FLOAT_WITHIN(5, 35.0, sensor->read_temperature(0));
}
TEST(temperature_ad7124, test_temperature_probe_t2)
{
    TEST_ASSERT_FLOAT_WITHIN(5, 35.0, sensor->read_temperature(1));
}
TEST(temperature_ad7124, test_temperature_probe_t3)
{
    TEST_ASSERT_FLOAT_WITHIN(5, 35.0, sensor->read_temperature(2));
}
TEST(temperature_ad7124, test_temperature_probe_t4)
{
    TEST_ASSERT_FLOAT_WITHIN(5, 35.0, sensor->read_temperature(3));
}
TEST(temperature_ad7124, test_temperature_probe_t5)
{
    TEST_ASSERT_FLOAT_WITHIN(5, 35.0, sensor->read_temperature(4));
}

TEST_GROUP_RUNNER(temperature_ad7124)
{
    RUN_TEST_CASE(temperature_ad7124, test_temperature_probe_t1)
    RUN_TEST_CASE(temperature_ad7124, test_temperature_probe_t2)
    RUN_TEST_CASE(temperature_ad7124, test_temperature_probe_t3)
    RUN_TEST_CASE(temperature_ad7124, test_temperature_probe_t4)
    RUN_TEST_CASE(temperature_ad7124, test_temperature_probe_t5)
}