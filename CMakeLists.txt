# The MIT License (MIT)
# 
# Copyright (c) 2020 Stefan Lüthi
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

## Project setup ###############################################################

cmake_minimum_required(VERSION 3.14)
project(espresso-master CXX C ASM)

set(CMAKE_C_STANDARD   11)
set(CMAKE_CXX_STANDARD 14)

if(${CMAKE_SYSTEM_PROCESSOR} MATCHES "arm")
    message("-- Building for embedded target")
    set(TARGET_SYSTEM "embedded")
else()
    message("-- Building for host")
    set(TARGET_SYSTEM "host")
endif()

# TODO: move to seperate file, isolate gcc specific flags
# Compiler and linker settings
set(COMPILER_COMMON "")
string(APPEND COMPILER_COMMON " -Wall -Wextra")
string(APPEND COMPILER_COMMON " -ffreestanding -fno-builtin")
string(APPEND COMPILER_COMMON " -ffunction-sections -fdata-sections")
string(APPEND COMPILER_COMMON " -fno-common -fmessage-length=0")
string(APPEND COMPILER_COMMON " -ffile-prefix-map=${CMAKE_SOURCE_DIR}/=/")

if(${TARGET_SYSTEM} MATCHES "embedded")
    set(COMPILER_CPP_OPT " -fno-exceptions -fno-rtti -fno-unwind-tables")
else(${TARGET_SYSTEM} MATCHES "host") 
    set(COMPILER_CPP_OPT " -fexceptions -fno-rtti -fno-unwind-tables")
endif()

set(LINKER_OPT " -Wl,--gc-sections")
set(LINKER_WRAP " -Wl,--wrap=malloc,--wrap=free")
set(LINKER_SIZE " -Wl,--print-memory-usage")

string(APPEND CMAKE_C_FLAGS "${COMPILER_COMMON}")
string(APPEND CMAKE_CXX_FLAGS "${COMPILER_COMMON} ${COMPILER_CPP_OPT}")
string(APPEND CMAKE_EXE_LINKER_FLAGS "${LINKER_OPT} ${LINKER_SIZE} ${LINKER_WRAP}")

string(APPEND CMAKE_C_FLAGS_DEBUG " -Os -g3")
string(APPEND CMAKE_C_FLAGS_RELEASE " -Os")
string(APPEND CMAKE_C_FLAGS_RELWITHDEBINFO " -Os -g3")
string(APPEND CMAKE_C_FLAGS_MINSIZEREL " -Os")

## Targets #####################################################################

# Empty target
set(TARGET_EMBEDDED ${CMAKE_PROJECT_NAME})
set(TARGET_UNIT_TESTS ${CMAKE_PROJECT_NAME}-unit-tests)
set(TARGET_INTEGRATION_TESTS ${CMAKE_PROJECT_NAME}-integration-tests)

if(${TARGET_SYSTEM} MATCHES "embedded")
    add_executable(${TARGET_EMBEDDED})
    add_executable(${TARGET_INTEGRATION_TESTS} EXCLUDE_FROM_ALL)
    set(CMAKE_EXECUTABLE_SUFFIX .elf)
    set_target_properties(
            ${TARGET_EMBEDDED} ${TARGET_INTEGRATION_TESTS}
            PROPERTIES
            arm TRUE
    )

else(${TARGET_SYSTEM} MATCHES "host") 
    add_executable(${TARGET_UNIT_TESTS})
endif()

## Dependencies ################################################################

add_subdirectory(board)
add_subdirectory(src)
add_subdirectory(dependencies)

if(${TARGET_SYSTEM} MATCHES "embedded")
    target_link_libraries(
        ${TARGET_EMBEDDED} PRIVATE 
        board::board
        src::src
        src::main
        src::rtos_interface 
        rtos::freertos_cpp
        devdrv::ad7124
        util::logger
        util::swo
    )

    target_link_libraries(
        ${TARGET_INTEGRATION_TESTS} PRIVATE 
        board::board
        src::src
        src::rtos_interface 
        rtos::freertos_cpp
        devdrv::ad7124
        util::logger
        util::swo
    )

else(${TARGET_SYSTEM} MATCHES "host") 
    target_link_libraries(
        ${TARGET_UNIT_TESTS} PRIVATE 
        board::mock
        src::src
        devdrv::ad7124
    )
endif()


## Test ########################################################################

enable_testing()
add_subdirectory(test)

if(${TARGET_SYSTEM} MATCHES "embedded")
    target_link_libraries(
        ${TARGET_INTEGRATION_TESTS} PRIVATE 
        test::integration
    )
    # add_test(${TARGET_INTEGRATION_TESTS})

else(${TARGET_SYSTEM} MATCHES "host") 
    target_link_libraries(
        ${TARGET_UNIT_TESTS} PRIVATE 
        test::unit
    )
    include(${catch_SOURCE_DIR}/contrib/Catch.cmake)
    catch_discover_tests(${TARGET_UNIT_TESTS})
endif()

## Package #####################################################################

if(${TARGET_SYSTEM} MATCHES "embedded")
    add_custom_command(
        TARGET ${TARGET_EMBEDDED} POST_BUILD
        COMMAND ${CMAKE_OBJCOPY} -O binary ${CMAKE_BINARY_DIR}/${TARGET_EMBEDDED}${CMAKE_EXECUTABLE_SUFFIX} ${CMAKE_BINARY_DIR}/${TARGET_EMBEDDED}.bin
        WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
        COMMENT "Convert binary files"
    )

    #set(OBJ_TO_DISASSEMBLY "${CMAKE_OBJDUMP};-C;-l;-d;")
    #set(OBJ_TO_DISASSEMBLY "echo;")
    #add_custom_command(
    #    TARGET ${TARGET_EMBEDDED} POST_BUILD
    #    #COMMAND_EXPAND_LISTS ${CMAKE_OBJCOPY} -d "$<TARGET_OBJECTS:${TARGET_EMBEDDED}>" -C -l > "$<TARGET_OBJECTS:${TARGET_EMBEDDED}>".s
    #    #COMMAND ${OBJ_TO_DISASSEMBLY} "$<JOIN:$<TARGET_OBJECTS:${TARGET_EMBEDDED}>,;&&;${OBJ_TO_DISASSEMBLY};>"
    #    #COMMAND echo "$<JOIN:$<TARGET_OBJECTS:${TARGET_EMBEDDED}>,;\>;dump.s;&&;${CMAKE_OBJDUMP};-C;-l;-d;>"
    #    COMMAND echo "$<JOIN:$<TARGET_OBJECTS:${TARGET_EMBEDDED}>,;&&;${OBJ_TO_DISASSEMBLY}>;"
    #    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
    #    COMMENT "Create disassembly from object files"
    #    COMMAND_EXPAND_LISTS
    #    OUTPUT_QUIET
    #)
endif()