# Espresso Master 2000

The espresso master 2000 is (or will be...) a control unit for an espresso machine.
The hardware has interfaces to read temperature, water flow and switches and can control the 
boiler, pump and solenoid valve.

The aim of this firmware project is to get familiar or extend knowledge with:
- C++
- OOP C
- testing
- CMake

for an embedded target (microcontroller).

## ! Important !
This project is currently in **hacking** state!

## Prerequisites

### Packages 

- ARM none eabi gcc/g++
- CMake
- CLion
    - SWO Plugin (which I haven't pushed to a public repo yet)
- or VS Code (project file not update anymore)
    - a shitload of plugins

### Repos

CMake toolchain:
```sh
cd /opt/embedded/toolchains
git clone git@gitlab.luethi.tech:toolchains/bare-metal.git
```

SVD files for ARM Cortex-M microcontrollers:
```sh
cd /opt/embedded/toolchains
git clone git@github.com:posborne/cmsis-svd.git
```

## Usage

### Command line build
#### Embedded target
```sh
mkdir build-embedded
cd build-embedded
cmake .. -DCMAKE_TOOLCHAIN_FILE=/opt/embedded/toolchains/bare-metal/arm_gcc_m4_hf.cmake
make -j5
```

Embedded integration tests are not built automatically:
```sh
make espresso-master-integration-tests
```

#### Host target (testing)
```sh
mkdir build
cd build
cmake ..
make -j5
./espresso-master-unit-tests
```

### SWO debug output

Enable SWO output to telnet (`localhost:2333`), after MCU is initialized:
```yaml
# in the gdb command line
# mon SWO EnableTarget <F_CPU> <F_SWO> <PORT_MASK> <MODE[0]>
mon SWO EnableTarget 0 0 0x2 0
# 0: auto detect F_CPU
# 0: fastet SWO clock possible
# 0x2: port 1 enabled
# 0: MODE can only be 0
```

With `.gdbinit` the ITM ports will be openend automatically on start of the 
debug session. For to work you need to tell gdb to read local gdbinit scripts:
```yaml
# ~/.gdbinit
set auto-load local-gdbinit on
add-auto-load-safe-path /
set pagination off
```

## Further documentation
- [Project structure](doc/project_structure.md)
- [CMake Templates](doc/cmake_template.md)

## Some ToDo's

- [ ] implement a C OOP class ([SourceEngineers: OOP in C](https://bitbucket.org/sourceengineers/oop-in-c/src/master/))
- [ ] implement some _real_ tests
- [ ] do some integration testing on PC
- [ ] does integration testing on MCU make sense? (-> HIL)
- [ ] clean-up toolchain file
- [ ] move compiler flags to separate file
- [ ] clean file headers
- [ ] clean submodules, ensure similar structure
- [ ] implement some useful features like a temperature controller :wink:
- [ ] move BSP to separate submodule